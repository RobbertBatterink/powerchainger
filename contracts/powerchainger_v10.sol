pragma solidity ^0.5.0;

contract powerchainger {

    struct Surplushouse {
        address surAddress;
        uint batterysize;
        uint currentcharge;
        uint spaceleft;
        uint donated;
        bool exists;
    }

    struct Demandhouse {
        address demandAddress;
        uint batterysize;
        uint currentcharge;
        uint spaceleft;
        uint tokenbalance;
        uint tokenspent;
        bool exists;
    }

    struct Nbattery {
        uint nbatterysize;
        uint nbatterycharge;
        uint nbatteryspace;
        uint totaldonated;
        uint tokenspent;
        uint tokensunspent;
        bool exists;
    }

    struct Donation {
        uint kwhamount;
        uint donationtime;
    }

    struct Donationref {
        uint[] donationids;
    }

    struct Redemption {
        uint tokens;
        uint redtime;
    }

    struct Redemptionref {
        uint[] redemptionids;
    }

    mapping (address => Surplushouse) surplushouses;
    mapping (address => Demandhouse) demandhouses;
    mapping (address => uint) balances;
    mapping (uint => Nbattery) nbatteries;
    mapping (address => mapping (uint => Donation)) donations;
    mapping (address => Donationref) donationrefs;
    mapping (address => mapping (uint => Redemption)) redemptions;
    mapping (address => Redemptionref) redemptionrefs;

    address[] public shouses;
    address[] public dhouses;

    address public owner;
    uint public nbatteryid;
    uint private donationid;
    uint private redemptionid;
    string public token;
    uint public tokensupplied;
    uint public dhousecount;

    constructor () public {
        owner = msg.sender;
        nbatteryid = 1;
        donationid = 1;
        redemptionid = 1;
        token = 'Energycoin';
        tokensupplied = 0;
        dhousecount = 0;
    }

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    modifier onlySurplus {
        require(surplushouses[msg.sender].exists);
        _;
    }

    modifier onlyDemand {
        require(demandhouses[msg.sender].exists);
        _;
    }

    function addNbattery(uint _nbatterysize, uint _nbatterycharge) onlyOwner public {
        require(_nbatterycharge <= _nbatterysize && !nbatteries[nbatteryid].exists);
        uint space = _nbatterysize - _nbatterycharge;
        nbatteries[nbatteryid] = Nbattery(
            {
             nbatterysize: _nbatterysize,
             nbatterycharge: _nbatterycharge,
             nbatteryspace: space,
             totaldonated: 0,
             tokenspent: 0,
             tokensunspent: 0,
             exists: true
            }
                                         );
    }

    //function for adding new demandhouses to the network, can only be accesses by contract owner
    function addDemandhouse(address _demandAddress, uint _batterysize, uint _currentcharge) onlyOwner public {
        require(!demandhouses[_demandAddress].exists);
        uint space = _batterysize - _currentcharge;
        demandhouses[_demandAddress] = Demandhouse(
            {
             demandAddress: _demandAddress,
             batterysize: _batterysize,
             currentcharge: _currentcharge,
             spaceleft: space,
             tokenbalance: 0,
             tokenspent: 0,
             exists: true
            }
                                                 );
        dhouses.push(_demandAddress);
        dhousecount += 1;
    }

    //function for adding new surplushouses to the network, can only be accesses by contract owner
    function addSurplushouse(address _surAddress, uint _batterysize, uint _currentcharge) onlyOwner public {
        require(!surplushouses[_surAddress].exists);
        uint space = _batterysize - _currentcharge;
        surplushouses[_surAddress] = Surplushouse(
            {
             surAddress: _surAddress,
             batterysize: _batterysize,
             currentcharge: _currentcharge,
             spaceleft: space,
             donated: 0,
             exists: true
            }
                                                 );
        shouses.push(_surAddress);
    }

    //function for surplushouses to add battery charge
    function setSurpluscharge(uint _amount) onlySurplus public {
        require(_amount <= surplushouses[msg.sender].batterysize);
        surplushouses[msg.sender].currentcharge = _amount;
        uint spaceleft = surplushouses[msg.sender].batterysize - surplushouses[msg.sender].currentcharge;
        surplushouses[msg.sender].spaceleft = spaceleft;
    }

    //function for surplushouses to add battery charge
    function setDemandcharge(uint _amount) onlyDemand public {
        require(_amount <= demandhouses[msg.sender].batterysize);
        demandhouses[msg.sender].currentcharge = _amount;
        uint spaceleft = demandhouses[msg.sender].batterysize - demandhouses[msg.sender].currentcharge;
        demandhouses[msg.sender].spaceleft = spaceleft;
    }


    //surplushouse donation function
    function donateEnergy(uint _amount) onlySurplus public {
        require(_amount <= nbatteries[nbatteryid].nbatteryspace && _amount <= surplushouses[msg.sender].currentcharge && nbatteries[nbatteryid].exists);
        nbatteries[nbatteryid].nbatterycharge += _amount;
        nbatteries[nbatteryid].nbatteryspace -= _amount;
        nbatteries[nbatteryid].totaldonated += _amount;
        nbatteries[nbatteryid].tokensunspent += _amount;
        surplushouses[msg.sender].donated += _amount;
        surplushouses[msg.sender].currentcharge -= _amount;
        surplushouses[msg.sender].spaceleft = surplushouses[msg.sender].batterysize - surplushouses[msg.sender].currentcharge;
        tokensupplied += _amount;

        Donation storage donation = donations[msg.sender][donationid];
        donation.kwhamount = _amount;
        donation.donationtime = block.timestamp;

        Donationref storage donationref = donationrefs[msg.sender];
        donationref.donationids.push(donationid);
        donationid++;

        uint tokenshare = _amount / dhousecount;
        for (uint i = 0; i < dhousecount; i++) {
            demandhouses[dhouses[uint(i)]].tokenbalance += tokenshare;
        }
    }

    function spendTokens(uint _amount) onlyDemand public {
        require(_amount <= demandhouses[msg.sender].tokenbalance
                && _amount <= demandhouses[msg.sender].spaceleft
                && _amount <= nbatteries[nbatteryid].nbatterycharge);

        demandhouses[msg.sender].tokenbalance -= _amount;
        demandhouses[msg.sender].currentcharge += _amount;
        demandhouses[msg.sender].spaceleft -= _amount;
        demandhouses[msg.sender].tokenspent += _amount;
        nbatteries[nbatteryid].nbatterycharge -= _amount;
        nbatteries[nbatteryid].nbatteryspace += _amount;
        nbatteries[nbatteryid].tokenspent += _amount;
        nbatteries[nbatteryid].tokensunspent -= _amount;

        Redemption storage redemption = redemptions[msg.sender][redemptionid];
        redemption.tokens = _amount;
        redemption.redtime = block.timestamp;

        Redemptionref storage redemptionref = redemptionrefs[msg.sender];
        redemptionref.redemptionids.push(redemptionid);
        redemptionid++;
    }

    //returns surplus household batterysize, current charge, space left in battery and total amount donated.
    //Restricted view, only surplus households can view their own details.
    function getSurhousedetails() onlySurplus public view returns (uint, uint, uint, uint) {
        return (surplushouses[msg.sender].batterysize,
                surplushouses[msg.sender].currentcharge,
                surplushouses[msg.sender].spaceleft,
                surplushouses[msg.sender].donated);
    }

    function getDemhousedetails() onlyDemand public view returns (uint, uint, uint, uint, uint) {
        return (demandhouses[msg.sender].batterysize,
                demandhouses[msg.sender].currentcharge,
                demandhouses[msg.sender].spaceleft,
                demandhouses[msg.sender].tokenbalance,
                demandhouses[msg.sender].tokenspent);
    }

    //returns the realt time details of the neigbourhood battery
    function getNBdetails() public view returns (uint, uint, uint, uint, uint, uint) {
        return (nbatteries[nbatteryid].nbatterysize,
                nbatteries[nbatteryid].nbatterycharge,
                nbatteries[nbatteryid].nbatteryspace,
                nbatteries[nbatteryid].totaldonated,
                nbatteries[nbatteryid].tokenspent,
                nbatteries[nbatteryid].tokensunspent);
    }

    function getDonation(uint _donationid) onlySurplus public view returns (uint, uint) {
        return (donations[msg.sender][_donationid].kwhamount,
                donations[msg.sender][_donationid].donationtime);
    }

    function getRedemption(uint _redemptionid) onlyDemand public view returns (uint, uint) {
        return (redemptions[msg.sender][_redemptionid].tokens,
                redemptions[msg.sender][_redemptionid].redtime);
    }

    // returns an array of public key addresses that are surplus households
    function getSurhouses() onlyOwner public view returns (address[] memory) {
        return shouses;
    }

    function getDemhouses() onlyOwner public view returns (address[] memory) {
        return dhouses;
    }

    //returns the public key address of the contract owner
    function getOwner() public view returns (address) {
        return owner;
    }

    function getDonationids() onlySurplus public view returns (uint[] memory) {
        return donationrefs[msg.sender].donationids;
    }

    function getRedemptionids() onlyDemand public view returns (uint[] memory) {
        return redemptionrefs[msg.sender].redemptionids;
    }

    function getTokendetails() public view returns (string memory, uint) {
        return (token, tokensupplied);
    }
}
