import React from "react";
import PieChart from 'react-minimal-pie-chart';

import Buying from './Buying'

class GetDemand extends React.Component {
    constructor(){
        super();
        this.state = {
            dataKey: null,
            demandDetails: [100, 50 , 50 ,0, 0],
        };

        this.getDemand = this.getDemand.bind(this);
    }

    //when component mounts call getDemand function
    componentDidMount() {
      this.getDemand();
    }

    //function to get the demand details from the blockchain
    getDemand() {
      const { drizzle } = this.props;
      const contract = drizzle.contracts.powerchainger;

      // let drizzle know we want to call the `getDemhousedetails` method
      const dataKey = contract.methods.getDemhousedetails().call();

      //parse data so we can use it
      var promise1 = Promise.resolve(dataKey);
      promise1.then(function(value) {
          return value;
      }).then(data => {
          this.setState({demandDetails: [data[0] / 1000, data[1] / 1000, data[2] / 1000, data[3] / 1000, data[4] / 1000]});
      });

      // save the `dataKey` to local component state for later reference
      this.setState({ dataKey: dataKey });
    }

    //render component
  render() {
    return (<div>
      <h2>Battery Size: {this.state.demandDetails[0]} kWh</h2>
      <h2>Your Balance: {this.state.demandDetails[3]} Tokens</h2>
      <h2>Tokens Spend: {this.state.demandDetails[4]} Tokens</h2>

      <PieChart
            data={[
                { title: "Space Left", value: this.state.demandDetails[2], color: '#bfbfbf' },
                { title: "Current Charge", value: this.state.demandDetails[1], color: '#19D209' },
            ]}
            totalValue={this.state.demandDetails[0]}
            lineWidth={20}
            label={({ data, dataIndex }) =>
                Math.round(data[dataIndex].percentage) + '%'
            }
            labelStyle={{
                fontSize: '10px',
                fontFamily: 'sans-serif'
            }}
            style={{height: '600px'}}
            labelPosition={60}
            rounded
            animate
        />
        <h2>Legend</h2>
        <table className='table'>
          <thead>
            <tr>
              <th scope='col'>Color</th>
              <th scope='col'>Name</th>
              <th scope='col'>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope='row' style={{backgroundColor: '#19D209'}}></th>
              <td>Current Charge</td>
              <td>{this.state.demandDetails[1]}</td>
            </tr>
            <tr>
              <th scope='row' style={{background: '#bfbfbf'}}></th>
              <td>Space Left</td>
              <td>{this.state.demandDetails[2]}</td>
            </tr>
          </tbody>
        </table>
        <Buying drizzle = {this.props.drizzle} drizzleState = {this.props.drizzleState}/>
      </div>);
  }
}

export default GetDemand;
