import React from "react";
import jwt_decode from 'jwt-decode'

class SetBatteryCharge extends React.Component {
  constructor() {
  super()
  this.state = {
      stackId: null,
      type: null,
      charge: 0,
   };

     this.handleChange = this.handleChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
  }

  //when component mounts get the user type and set the state for type loading and drizzle
  componentDidMount(){
      const token = localStorage.getItem('usertoken')
      if(token != null && !this.state.didUpdate ){
          this.setState({didUpdate: true});
          const decode = jwt_decode(token)
          this.setState({
              type: decode.identity.type,
          })
      }

      this.setState({
          loading: this.props.loading,
          drizzleState: this.props.drizzleState,
      })
  }

  //on change handle state change
   handleChange(e) {
       var name = e.target.getAttribute('name')
       this.setState({[name]: e.target.value});
   }

   //on submit call setBCvalue
   handleSubmit(e) {
       //write to blockchain
       const charge = this.state.charge * 1000;

       this.setBCValue(charge);

   }

   //function to write to the blockchain
  setBCValue(charge) {
      const { drizzle, drizzleState } = this.props;
      const contract = drizzle.contracts.powerchainger;

      if(this.state.type === 1){
          const stackId = contract.methods["setSurpluscharge"].cacheSend(charge, {from: drizzleState.accounts[0]});
             this.setState({stackId: stackId});
       } else if(this.state.type === 2) {
           const stackId = contract.methods["setDemandcharge"].cacheSend(charge, {from: drizzleState.accounts[0]});
           this.setState({stackId: stackId});
       }

  }


  getTxStatus = () => {
    // get the transaction states from the drizzle state
    const { transactions, transactionStack } = this.props.drizzleState;

    // get the transaction hash using our saved `stackId`
    const txHash = transactionStack[this.state.stackId];

    // if transaction hash does not exist, don't display anything
    if (!txHash) return null;

    // otherwise, return the transaction status
    return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
  };

  //render component
  render() {
    return (
      <div className='container'>
        <h1 className="h3 mb-3 font-weight-normal">Set new charge for the battery</h1>
        <hr />
        <form onSubmit={this.handleSubmit}>
            <table className='table'>
            <tr>
            <label>
                Set Charge:
                <input type="text" class="form-control" name="charge" onChange={this.handleChange} value={this.state.charge} />
            </label>
            </tr>
            </table>
            <input type="submit" class="btn btn-primary" value="submit" />
        </form>
      </div>
    );
  }
}

export default SetBatteryCharge;
