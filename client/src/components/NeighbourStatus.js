import React, {Component} from 'react'

import GetBattery from "./GetNBdetails";

class NeigbourhoodStatus extends Component {
    constructor(){
        super();
        this.state = {
            loading: true,
            drizzleState: null,
        };
    }

    //when component mouts set the state for loading and drizzle
    componentDidMount(){
        this.setState({
            loading: this.props.loading,
            drizzleState: this.props.drizzleState,
        })
    }

    //render component
    render() {
        if (this.state.loading) return "Loading Drizzle...";
        return (
            <div className="container">
                <div className="jumbotron mt-5 text-center">
                    <h1 className="h3 mb-3 font-weight-normal">See neigbourhood status</h1>
                    <div class="d-flex justify-content-center">
                        <GetBattery
                          drizzle={this.props.drizzle}
                          drizzleState={this.state.drizzleState}
                         />
                 </div>
             </div>
         </div>
        )
    }
}

export default NeigbourhoodStatus;
