import React, {Component} from 'react'

import SetSpendTokens from "./SetSpendTokens";
import SetDonateEnergy from "./SetDonateEnergy";

class Selling extends Component {
    constructor(){
        super();
        this.state = {
            loading: true,
            drizzleState: null,
            battery: [],
        };
    }

    //when component mounts get data from props
    componentDidMount(){
        this.setState({
            loading: this.props.loading,
            drizzleState: this.props.drizzleState,
            battery: this.props.battery,
        })
    }

    //render component
    render() {
        if (this.state.loading) return "Loading Drizzle...";
        return (
            <div className="container">
                <div className="jumbotron mt-5 text-center">
                    <h1 className="h3 mb-3 font-weight-normal">Donate energy to the demandhouseholds</h1>
                    <div class="d-flex justify-content-center">
                        <SetDonateEnergy
                          drizzle={this.props.drizzle}
                          drizzleState={this.state.drizzleState}
                          battery={this.state.battery}
                         />
                 </div>
             </div>
         </div>
        )
    }
}

export default Selling;
