import React from "react";

import Typography from '@material-ui/core/Typography';

class GetDonation extends React.Component {
    constructor(){
        super();
        this.state = {
            dataKey: null,
            date: null,
            donation: [],
            loading: true,
            drizzleState: null,
        };

        this.getDonation = this.getDonation.bind(this);
    }

    //When component mounts get the id from props and call getDonation fuction
    //and set the state for loading and drizzle
    componentDidMount() {
        const id = this.props.id;

        console.log(this.props)
        this.getDonation(id);
        this.setState({
            loading: this.props.loading,
            drizzleState: this.props.drizzleState,
        })


    }

    //Function the get donation detials via an id
    getDonation(id) {
      const { drizzle } = this.props;
      const contract = drizzle.contracts.powerchainger;

      // let drizzle know we want to call the `getDonation` method
      const dataKey = contract.methods.getDonation(id).call();

      //parse data so we can use it
      var promise1 = Promise.resolve(dataKey);
      promise1.then(function(value) {
          return value;
      }).then(data => {
          this.setState({donation: [data[0] / 1000, data[1]]});
          //call setTime method so we have a readable timestamp
          this.setTime(data[1]);
      });

      // save the `dataKey` to local component state for later reference
      this.setState({ dataKey: dataKey });
    }

    //converts timestamp into something readable
    setTime(time) {
        const transDate = new Date(time * 1000);
        const stringDate = transDate.toString();
        console.log(stringDate);
        this.setState({ date: stringDate });
        console.log(this.state)
    }

//render component
  render() {
    return (<Typography> <table ><tr><td>kwh</td><td>=</td><td align='left'>{this.state.donation[0]}</td></tr><tr><td>time</td><td>=</td><td>{this.state.date}</td></tr></table> </Typography>);
  }
}

export default GetDonation;
