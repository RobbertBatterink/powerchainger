import React from "react";

import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import GetRedemption from "./GetRedemption"


class GetRedemptionids extends React.Component {
    constructor(){
        super();
        this.state = {
            dataKey: null,
            redemptionids: [],
            loading: true,
            drizzleState: null,
        };

        this.getRedemptionids = this.getRedemptionids.bind(this);
    }

    //when component mounts call getRedemptionids function and set the state for loading and drizzle
    componentDidMount() {
        this.getRedemptionids();
        this.setState({
            loading: this.props.loading,
            drizzleState: this.props.drizzleState,
        })
    }

    //function to get all the redemption ids from the blockchain
  getRedemptionids() {
    const { drizzle } = this.props;
    const contract = drizzle.contracts.powerchainger;

    // let drizzle know we want to call the `getRedemptionids` method
    const dataKey = contract.methods.getRedemptionids().call();
    // parse data so we can use it
    var promise1 = Promise.resolve(dataKey);
    promise1.then(function(value) {
        return value;
    }).then(data => {
        this.setState({redemptionids: data});
    });

    // save the `dataKey` to local component state for later reference
    this.setState({ dataKey: dataKey });
  }

  //render component
  render() {

  const useStyles = makeStyles(theme => ({
    root: {
    width: '100%',
    },
    heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    },
    }));

    //list item filled with the redemption data
  const RedemptionListItem = ({value, id, drizzle, drizzleState}) => {

    const classes = useStyles();

    return (
      <ExpansionPanel>
         <ExpansionPanelSummary
           expandIcon={<ExpandMoreIcon />}
           aria-controls="panel1a-content"
           id={id}
         >
           <Typography className={classes.heading}>Redemption {value}</Typography>
         </ExpansionPanelSummary>
         <ExpansionPanelDetails>
            <GetRedemption id={value} drizzle={drizzle} drizzleState={drizzleState}/>
        </ExpansionPanelDetails>
       </ExpansionPanel>
   )
  }

  //list filled with the list items
  const RedemptionList = ({ids, drizzle, drizzleState}) => {

      const classes = useStyles();

      return (
      <div className={classes.root}>
          { ids.map((id, i) => <RedemptionListItem key={i} value={id} id={ids[i]} drizzle={drizzle} drizzleState={drizzleState} />)}
      </div>
    )
  };

    return (
      <RedemptionList ids={this.state.redemptionids} drizzle={this.props.drizzle} drizzleState={this.state.drizzleState}/>
      );
  }
}

export default GetRedemptionids;
