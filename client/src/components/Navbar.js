import React, {Component} from 'react'
import {Link, withRouter} from 'react-router-dom'
import jwt_decode from 'jwt-decode'
import './Navbar.css';

import $ from 'jquery';

import logo from './BETA-LOGOv2.png'

class Navbar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            type: null,
            first_name: null,
            didUpdate: false,
        }
    }

    logOut(e) {
        e.preventDefault();
        localStorage.removeItem('usertoken');
        this.setState({ type: null, didUpdate: false});
        this.props.history.push('/');
    }

    componentDidMount() {
        const token = localStorage.getItem('usertoken')
        if(token != null && !this.state.didUpdate ){
            this.setState({didUpdate: true});
            const decode = jwt_decode(token)
            this.setState({
                type: decode.identity.type,
                first_name: decode.identity.first_name,
            })
        }
    }

    componentDidUpdate() {
        const token = localStorage.getItem('usertoken')
        if(token != null && !this.state.didUpdate ){
            this.setState({didUpdate: true});
            const decode = jwt_decode(token)
            this.setState({
                type: decode.identity.type,
                first_name: decode.identity.first_name,
            })
        }
    }

    render() {
        const LoginRegLink = () => {
            return (
                <ul className="navbar-nav" >
                    <li className="nav-item">
                        <Link className="nav-link" to="/login"  style={{color: "#479FB2"}}>Login</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link glyphicon glyphicon-user" to="/register"  style={{color: "#479FB2"}}>Register</Link>
                    </li>
                </ul>
            )
        }

        const DemandUserLink = () => {
            return (
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link className="nav-link" to="/profile" style={{color: "#479FB2"}} >{this.state.first_name}</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/buy" style={{color: "#479FB2"}}> Buy Energy</Link>
                    </li>
                    <li className="dropdown">
                        <Link className="nav-link dropdown-toggle" data-toggle="dropdown" style={{color: "#479FB2"}}> Status </Link>
                        <ul class="dropdown-menu" style={{backgroundColor: "white"}}>
                            <li className="nav-item">
                                <Link className="nav-link" to="/housestatus" style={{color: "#479FB2"}}>House Status</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/neigbourhoodstatus" style={{color: "#479FB2"}}> Neigbourhood Status</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/transactions" style={{color: "#479FB2"}}> Transactions</Link>
                            </li>
                        </ul>
                    </li>
                    <li className="nav-item navbar-right">
                        <a href="" onClick={this.logOut.bind(this)} className="nav-link" style={{color: "#479FB2"}}>Logout</a>
                    </li>
                </ul>
            )
        }

        const SurplusUserLink = () => {
            return (
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link className="nav-link" to="/profile" style={{color: "#479FB2"}}>{this.state.first_name}</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/donate" style={{color: "#479FB2"}}> Donate Energy</Link>
                    </li>
                    <li className="dropdown">
                        <Link className="nav-link dropdown-toggle" data-toggle="dropdown" style={{color: "#479FB2"}}> Status </Link>
                        <ul class="dropdown-menu" style={{backgroundColor: "white"}}>
                            <li className="nav-item">
                                <Link className="nav-link" to="/housestatus" style={{color: "#479FB2"}}>House Status</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/neigbourhoodstatus" style={{color: "#479FB2"}}> Neigbourhood Status</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/transactions" style={{color: "#479FB2"}}> Transactions</Link>
                            </li>
                        </ul>
                    </li>
                    <li className="nav-item navbar-right">
                        <a href="" onClick={this.logOut.bind(this)} className="nav-link" style={{color: "#479FB2"}}>Logout</a>
                    </li>
                </ul>
            )
        }

        const EfundUserLink = () => {
            return (
                <ul className="navbar-nav" >
                    <li className="nav-item">
                        <Link className="nav-link" to="/profile" style={{color: "#479FB2"}}>{this.state.first_name}</Link>
                    </li>
                    <li className="dropdown">
                        <Link className="nav-link dropdown-toggle" data-toggle="dropdown" style={{color: "#479FB2"}}> Add </Link>
                        <ul class="dropdown-menu" style={{backgroundColor: "white"}}>
                            <li className="nav-item">
                                <Link className="nav-link" to="/addhouse" style={{color: "#479FB2"}}>Add Household</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/addbattery" style={{color: "#479FB2"}}> Add Battery</Link>
                            </li>
                        </ul>
                    </li>
                    <li className="dropdown">
                        <Link className="nav-link dropdown-toggle" data-toggle="dropdown" style={{color: "#479FB2"}}> Status </Link>
                        <ul class="dropdown-menu" style={{backgroundColor: "white"}}>
                            <li className="nav-item">
                                <Link className="nav-link" to="/neigbourhoodstatus" style={{color: "#479FB2"}}> Neigbourhood Status</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/transactions" style={{color: "#479FB2"}}> Transactions</Link>
                            </li>
                        </ul>
                    </li>
                    <li className="nav-item navbar-right">
                        <a href="" onClick={this.logOut.bind(this)} className="nav-link" style={{color: "#479FB2"}}>Logout</a>
                    </li>
                </ul>
            )
        }

        const NavbarItems = () => {
            if(this.state.type === null) {
                return <LoginRegLink />;
            }
            else if(this.state.type === 2){
                return <DemandUserLink />;
            }else if(this.state.type === 1){
                return <SurplusUserLink />;
            }else if(this.state.type === 3){
                return <EfundUserLink />;
            }else {
                return null;
            }
        }

        return (
            <nav className="navbar fixed-top navbar-expand-lg navbar-light" style={{backgroundColor: "white", height: "100px"}}>
                <a style={{position: 'absolute'}} class="navbar-brand" href="http://www.powerchainger.nl/">
                    <img  src={logo} width="156" height="46"/>
                </a>
                <div></div>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1" aria-controls="navbar1" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-md-center navbar-ligth rounded" id="navbar1" style={{backgroundColor: "white", paddingLeft: '10%', paddingRight: '10%'}}>
                    <ul className="navbar-nav">
                        <li className="nav-item" class="navcolor">
                            <Link to="/#home" className="nav-link" style={{color: "#479FB2"}}>
                              Home
                            </Link>
                        </li>
                        <li className="nav-item"  href="#about" class="navcolor">
                            <Link to="/#about" className="nav-link" style={{color: "#479FB2"}}>
                                About
                            </Link>
                        </li>
                        <li className="nav-item" class="navcolor">
                            <Link to="/#contacts" className="nav-link" style={{color: "#479FB2"}}>
                              Contacts
                            </Link>
                        </li>
                    </ul>
                    <NavbarItems />
                </div>
            </nav>
        )
    }
}

export default withRouter(Navbar)
