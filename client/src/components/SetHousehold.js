import React from "react";

class SetHousehold extends React.Component {
  constructor() {
  super()
  this.state = {
      stackId: null,
      Type: "Demand",
      Address: "",
      BatterySize: 0,
      CurrentCharge: 0,
   };

     this.handleChange = this.handleChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
  }

  //on change handle state change
   handleChange(e) {
       var name = e.target.getAttribute('name')
       this.setState({[name]: e.target.value});
   }

   //on submit call setHHvalue
   handleSubmit(e) {
       //write to blockchain
       const size = this.state.BatterySize * 1000;
       const charge = this.state.CurrentCharge * 1000;

       this.setHHValue(this.state.Type, this.state.Address, size, charge);

   }

   //function to write to the blockchain
  setHHValue(type, address, size, charge) {
      const { drizzle, drizzleState } = this.props;
      const contract = drizzle.contracts.powerchainger;

      if(type == "Surplus"){
          const stackId = contract.methods["addSurplushouse"].cacheSend(address,
              size, charge, {from: drizzleState.accounts[0]});
             this.setState({stackId: stackId});
       } else if(type == "Demand") {
           const stackId = contract.methods["addDemandhouse"].cacheSend(address,
               size, charge, {from: drizzleState.accounts[0]});
           this.setState({stackId: stackId});
       }

  }


  getTxStatus = () => {
    // get the transaction states from the drizzle state
    const { transactions, transactionStack } = this.props.drizzleState;

    // get the transaction hash using our saved `stackId`
    const txHash = transactionStack[this.state.stackId];

    // if transaction hash does not exist, don't display anything
    if (!txHash) return null;

    // otherwise, return the transaction status
    return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
  };

  //render component
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
            <table>
            <tr>
            <label>
                Type:
                <select type="text" class="form-control" name="Type" onChange={this.handleChange} value={this.state.Type}>
                    <option>Demand</option>
                    <option>Surplus</option>
                </select>
            </label>
            </tr>
            <tr>
            <label>
                Address:
                <input type="text" class="form-control" name="Address" onChange={this.handleChange} value={this.state.Address} />
            </label>
            </tr>
            <tr>
            <label>
                Battery size:
                <input type="text" class="form-control" name="BatterySize" onChange={this.handleChange} value={this.state.BatterySize} />
            </label>
            </tr>
            <tr>
            <label>
                Current charge:
                <input type="text" class="form-control" name="CurrentCharge" onChange={this.handleChange} value={this.state.CurrentCharge} />
            </label>
            </tr>
            </table>
            <input type="submit" class="btn btn-primary" value="submit" />
        </form>
      </div>
    );
  }
}

export default SetHousehold;
