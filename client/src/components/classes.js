export class Household {
    constructor(type, demand, batCharge, batMaxCap) {
        this.type = type;
        this.demand = demand;
    }

    returnType() {
        console.log("i am a " + this.type);
    }

    returnDemand() {
        console.log("i need: " + this.demand + " kwh" )
    }
}

export class Demand extends Household {
    constructor(type, demand, batCharge, batMaxCap) {
        super(type, demand, batCharge, batMaxCap);
    }
}

export class Surplus extends Household {
    constructor(type, demand, batCharge, batMaxCap, energyProduction) {
        super(type, demand, batCharge, batMaxCap);
        this.energyProduction = energyProduction;
    }

    returnType() {
        console.log("i am a " + this.type);
    }

    sendEnergy(amount) {
        console.log("sending " + amount);
    }
}

export class Battery {
    constructor(charge, maxCapacity) {
        this.charge = charge;
        this.maxCapacity = maxCapacity;
    }

    returnBatteryStatus() {
        console.log("my current charge is: " + this.charge);
        console.log("my max capacity is: " + this.maxCapacity);
    }

    chargeBattery(amount) {
        if(amount > (this.maxCapacity - this.charge)) {
            console.log("too much energy");
        }else {
            this.charge += amount;
            console.log("my current charge is: " + this.charge);
        }
    }

    disChargeBattery(amount) {
        if(amount > this.charge) {
            console.log("too much energy");
        }else {
            this.charge -= amount;
            console.log("my current charge is: " + this.charge);
        }
    }
}

class Network {
    constructor(time, totalLoad) {
        this.time = time;
        this.totalLoad = totalLoad;
    }

    checkLoad(){
        console.log("checkLoad of all cables")
    }
}

class Cable {
    constructor(load, safeLoad, state) {
        this.load = load;
        this.safeLoad = safeLoad;
        this.state = state;
    }

    increaseLoad(amount) {
        this.load += amount;
        console.log(this.load);
        this.checkState();
    }

    decreaseLoad(amount) {
        this.load -= amount;
        this.checkState();
    }

    checkState(){
        if(207 < this.load && this.load < 253) {
            this.state = "green";
        } else if (this.safeload*0.8 > this.load || this.load > this.safeLoad*1.2) {
            this.state = "red";
        } else {
            this.state = "orange";
        }
        console.log(this.state);
    }
}

class DSO {
    makeRequest(household) {
        console.log("send request to: " + household)
    }
}



let d = new Demand("demand", 200);
let s = new Surplus("surplus", 200);
let b = new Battery(0, 200);
let c1 = new Cable(230, 230, "green");
let dso = new DSO();
let n = new Network();

d.returnType();
d.returnDemand();
s.returnType();
s.returnDemand();
s.sendEnergy(50);
b.returnBatteryStatus();
b.chargeBattery(10);
b.disChargeBattery(20);
b.returnBatteryStatus();
c1.checkState();
c1.increaseLoad(10);
c1.increaseLoad(20);
c1.increaseLoad(20);
dso.makeRequest("house1");
n.checkLoad();
