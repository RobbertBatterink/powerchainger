import React from "react";

import Typography from '@material-ui/core/Typography';

class GetRedemption extends React.Component {
    constructor(){
        super();
        this.state = {
            dataKey: null,
            redemption: [],
            date: null,
            loading: true,
            drizzleState: null,
        };

        this.getRedemption = this.getRedemption.bind(this);
    }

    //when component mount get id from props and use it to call getRedemption functions
    //and set the state for loading and drizzle
    componentDidMount() {
        const id = this.props.id;

        console.log(this.props)
        this.getRedemption(id);
        this.setState({
            loading: this.props.loading,
            drizzleState: this.props.drizzleState,
        })
    }

    //function the get the redemption detials from the blockchain
  getRedemption(id) {
    const { drizzle } = this.props;
    const contract = drizzle.contracts.powerchainger;

    // let drizzle know we want to call the `getRedemption` method
    const dataKey = contract.methods.getRedemption(id).call();
    //parse data so we can use it
    var promise1 = Promise.resolve(dataKey);
    promise1.then(function(value) {
        return value;
    }).then(data => {
        this.setState({redemption: [data[0] / 1000, data[1]]});
        this.setTime(data[1]);
    });

    // save the `dataKey` to local component state for later reference
    this.setState({ dataKey: dataKey });
  }
  //change the timestamp into something readable
  setTime(time) {
      const transDate = new Date(time * 1000);
      const stringDate = transDate.toString();
      console.log(stringDate);
      this.setState({ date: stringDate });
      console.log(this.state)
  }

  //render component
  render() {
    return (<Typography> <table ><tr><td>Tokens</td><td>=</td><td align='left'>{this.state.redemption[0]}</td></tr><tr><td>time</td><td>=</td><td>{this.state.date}</td></tr></table> </Typography>);
  }
}

export default GetRedemption;
