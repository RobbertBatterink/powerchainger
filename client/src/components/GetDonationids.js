import React from "react";

import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import GetDonation from "./GetDonation"


class GetDonationids extends React.Component {
    constructor(){
        super();
        this.state = {
            dataKey: null,
            donationids: [],
            loading: true,
            drizzleState: null,
        };

        this.getDonationids = this.getDonationids.bind(this);
    }

    //when component mounts call getDonationids and set the state for loading and drizzle
    componentDidMount() {
        this.getDonationids();
        this.setState({
            loading: this.props.loading,
            drizzleState: this.props.drizzleState,
        })
    }

    //function to get all the donation ids from the blockchain
    getDonationids() {
      const { drizzle } = this.props;
      const contract = drizzle.contracts.powerchainger;

      // let drizzle know we want to call the `getDonationids` method
      const dataKey = contract.methods.getDonationids().call();

      //parse data so we can use it
      var promise1 = Promise.resolve(dataKey);
      promise1.then(function(value) {
          console.log(value)
          return value;
      }).then(data => {
          this.setState({donationids: data});
      });

      // save the `dataKey` to local component state for later reference
      this.setState({ dataKey: dataKey });
    }

    //render component
  render() {
  const useStyles = makeStyles(theme => ({
    root: {
    width: '100%',
    },
    heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    },
    }));

    //A list item cotaining the donation data
  const DonationListItem = ({value, id, drizzle, drizzleState}) => {

    const classes = useStyles();

    return (
      <ExpansionPanel>
         <ExpansionPanelSummary
           expandIcon={<ExpandMoreIcon />}
           aria-controls="panel1a-content"
           id={id}
         >
           <Typography className={classes.heading}>Donation {value}</Typography>
         </ExpansionPanelSummary>
         <ExpansionPanelDetails>
            <GetDonation id={value} drizzle={drizzle} drizzleState={drizzleState}/>
        </ExpansionPanelDetails>
       </ExpansionPanel>
   )
  }

  //the list containing the list items
  const DonationList = ({ids, drizzle, drizzleState}) => {

      const classes = useStyles();

      return (
      <div className={classes.root}>
          { ids.map((id, i) => <DonationListItem key={i} value={id} id={ids[i]} drizzle={drizzle} drizzleState={drizzleState} />)}
      </div>
    )
  };

    return (
      <DonationList ids={this.state.donationids} drizzle={this.props.drizzle} drizzleState={this.state.drizzleState}/>
      );
  }
}

export default GetDonationids;
