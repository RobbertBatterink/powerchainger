import React, {Component} from 'react'
import {Link} from 'react-router-dom'

import logo from './pc1.png'

class Footer extends Component {

    //render component
    render() {
        return (
            <div style={{height: 300, backgroundColor: '#A09C40', paddingTop: 50, color: 'white', left: 0, bottom: 0, right: 0}}>
                <div className="container" >
                <img src={logo} />
                <hr style={{backgroundColor: 'white', marginTop: 75}}/>
                <div className="bottom_info d-flex justify-content-between">
                    <div class="pull-left">
                        Copyright  <Link href="www.powerchainger.nl" style={{color: "white"}}>Powerchainger</Link> 2019.
                    </div>
                    <div class="pull-right">
                        <a href="www.powerchainger.nl" style={{color: "white"}}>Terms and Conditions</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.powerchainger.nl/privacy/" style={{color: "white"}}>Privacy</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="www.powerchainger.nl/contact" style={{color: "white"}}>Contact</a>
                    </div>
                </div>
                </div>
            </div>
        )
    }
}

export default Footer;
