import React, {Component} from 'react'
import {login} from './UserFunctions'
import { Redirect } from 'react-router'

class Login extends Component {
    constructor(){
        super()
        this.state = {
            email: '',
            password: '',
            redirect: false
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    //onchange change the state
    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    //if login doens't giva an error do redirect
    onSubmit(e){
        e.preventDefault()

        const user = {
            email: this.state.email,
            password: this.state.password
        }

        login(user).then(res => {
            if(!res.error) {
                this.setState({redirect: true})
                //this.props.history.push('/profile')
            }
        })
    }

    //render component
    render() {

        if(this.state.redirect) {
            return <Redirect to='/profile' />
        }
        return (
            <div className="container" style={{color: '#479FB2'}}>
                <div className="jumbotron mt-5">
                    <div className="row">
                        <div className="col-md-6 mt-5 mx-auto">
                            <form noValidate onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label htmlFor="email">Email: </label>
                                    <input type="email" className="form-control" name="email" placeholder="Enter email" value={this.state.email} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Password: </label>
                                    <input type="password" className="form-control" name="password" placeholder="Enter password" value={this.state.password} onChange={this.onChange}/>
                                </div>
                                <button type="submit" className="btn btn-lg btn-primary btn-block">Login</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login
