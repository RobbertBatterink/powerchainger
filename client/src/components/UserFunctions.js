import axios from 'axios'

export const register = newUser => {
    return axios
    .post('users/register', {
        first_name: newUser.first_name,
        last_name: newUser.last_name,
        email: newUser.email,
        password: newUser.password,
        phone_number: newUser.phone_number,
        address: newUser.address,
        zip_code: newUser.zip_code,
        city: newUser.city,
        type: newUser.type     
    })
    .then(res => {
        console.log(res)
    })
}

export const login = user => {
    return axios
    .post('users/login', {
        email: user.email,
        password: user.password
    })
    .then(res => {
        localStorage.setItem('usertoken', res.data.token)
        return res.data
    })
    .catch(err => {
        console.log(err)
    })
}
