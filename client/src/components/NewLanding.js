import React, {Component} from 'react'

import {Link, withRouter} from 'react-router-dom'
import { FaSignInAlt, FaUserPlus, FaHome, FaChartPie } from "react-icons/fa";

import Navbar from './Navbar'
import Login from './Login'
import Register from './Register'

import $ from 'jquery'
import PieChart from 'react-minimal-pie-chart';

import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';

import { makeStyles, useTheme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';

import pic from './picture.jpg'

//import { Document, Page, pdfjs } from 'react-pdf'

//light blue #BADBEA
//steel blue #479FB2
//dark slate gray #1D3430
//dark khaki #A09C40
//medium aquamarine #7AB7C6

class NewLanding extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loaded: true,
            loginTab: "nav-link active",
            loginTab2: "tab-pane fade show active",
            loginColor: "#479FB2",
            registerTab: "nav-link",
            registerTab2: "tab-pane fade",
            registerColor: "#BADBEA",
        }
        console.log(this.props)
        this.clickLogin = this.clickLogin.bind(this);
        this.clickRegister = this.clickRegister.bind(this);
    }

    //when component mounts call clicklogin function
    componentDidMount() {
        this.clickLogin()
    }

    //state changes
    clickLogin() {
        this.setState({
            loginTab: "nav-link active",
            loginTab2: "tab-pane fade show active rounded-bottom",
            loginColor: "#479FB2",
            registerTab: "nav-link",
            registerTab2: "tab-pane fade",
            registerColor: "#BADBEA",
        })
    }

    //state changes
    clickRegister() {
        this.setState({
            loginTab: "nav-link",
            loginTab2: "tab-pane fade",
            loginColor: "#BADBEA",
            registerTab: "nav-link active",
            registerTab2: "tab-pane fade show active rounded-bottom",
            registerColor: "#479FB2",
        })
    }

    //render component
    render() {

      const useStyles = makeStyles({
        card: {
          minWidth: 357,
          color: '#A09C40',
          backgroundColor: "#479FB2",
        },
        media: {
          height: 50,
          color: 'white',
        },
        picture: {
          width: 235,
          height: 235,
          paddingLeft: 55,
          color: 'white',
        },
        root: {
          backgroundColor: 'white',
          width: '100%',
        },
      });


      const BootTabs = () => {
          return (
              <div>
                  <ul class="nav nav-tabs rounded text-center" id="myTab" role="tablist">
                    <li class="nav-item " style={{width: '50%'}}>
                      <a class={this.state.loginTab} id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true" onClick={this.clickLogin} style={{borderColor: this.state.loginColor, backgroundColor: this.state.loginColor, color: 'white'}}>Login</a>
                    </li>
                    <li class="nav-item" style={{width: '50%'}}>
                      <a class={this.state.registerTab} id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="false" onClick={this.clickRegister} style={{borderColor: this.state.registerColor, backgroundColor: this.state.registerColor, color: 'white'}} >Register</a>
                    </li>
                  </ul>
                  <div class="tab-content rounded text-center" id="myTabContent">
                    <div class={this.state.loginTab2} id="login" role="tabpanel" aria-labelledby="login-tab" style={{backgroundColor: '#479FB2', color: 'white', padding: '1%'}}>
                        <h1>Please Login </h1>
                        <Login />
                    </div>
                    <div class={this.state.registerTab2} id="register" role="tabpanel" aria-labelledby="register-tab" style={{backgroundColor: '#479FB2', color: 'white', padding: '1%'}}>
                        <h1>Register</h1>
                        <Register />
                    </div>
                  </div>
              </div>
          );
      }

        return (
            <div id="home" style={{paddingTop: 100}}>
                <div className="pic" style={{background: "url(" + pic + ")", height: '600px', justifyContent: 'center', alignItems: 'center'}}>
                    <img src={pic} style={{position: 'absolute', height: 600, width: '100%'}} />
                        <div className="container center text-center" style={{zIndex: 10, position: 'relative', color: 'white', justifyContent: 'center', alignItems: 'center', paddingTop: 25}}>
                            <h1>POWERCHAINGER</h1>
                            <h3>WEB-BASED SOCIAL ENERGY MARKET</h3>
                        </div>
                    </div>
                <div className="container" style={{paddingBottom: 50, paddingTop: 50}}>
                  <BootTabs />
                </div>
            </div>
        )
    }
}

export default NewLanding
