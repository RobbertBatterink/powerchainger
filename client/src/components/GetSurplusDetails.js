import React from "react";
import PieChart from 'react-minimal-pie-chart';

import Selling from "./Selling";
import SetBatteryCharge from "./SetBatteryCharge";

class GetSurplus extends React.Component {
    constructor(){
        super();
        this.state = {
            dataKey: null,
            surplusDetails: [100, 50, 50, 0],
        };

        this.getSurplus = this.getSurplus.bind(this);
    }

    //when component mounts call getSurplus function
    componentDidMount() {
      this.getSurplus();
    }

  //function to get the surplus details
  getSurplus() {
    const { drizzle } = this.props;
    const contract = drizzle.contracts.powerchainger;

    // let drizzle know we want to call the `getSurhousedetails` method
    const dataKey = contract.methods.getSurhousedetails().call();

    //parse data so we can use it
    var promise1 = Promise.resolve(dataKey);
    promise1.then(function(value) {
        return value;
    }).then(data => {
        this.setState({surplusDetails: [data[0] / 1000, data[1] / 1000, data[2] / 1000, data[3] / 1000]});
    });

    // save the `dataKey` to local component state for later reference
    this.setState({ dataKey: dataKey });
  }

  //render component
  render() {
    return (<div>
      <h2>Battery Size: {this.state.surplusDetails[0]} kWh</h2>
      <h2>Energy Donated: {this.state.surplusDetails[3]} kWh</h2>
      <PieChart
            data={[
                { title: "Space Left", value: this.state.surplusDetails[2], color: '#bfbfbf' },
                { title: "Current Charge", value: this.state.surplusDetails[1], color: '#19D209' },
            ]}
            totalValue={this.state.surplusDetails[0]}
            lineWidth={20}
            label={({ data, dataIndex }) =>
                Math.round(data[dataIndex].percentage) + '%'
            }
            labelStyle={{
                fontSize: '10px',
                fontFamily: 'sans-serif'
            }}
            style={{height: '500px'}}
            labelPosition={60}
            rounded
            animate
        />
        <h2>Legend</h2>
        <table className='table'>
          <thead>
            <tr>
              <th scope='col'>Color</th>
              <th scope='col'>Name</th>
              <th scope='col'>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope='row' style={{backgroundColor: '#19D209'}}></th>
              <td>Current Charge</td>
              <td>{this.state.surplusDetails[1]}</td>
            </tr>
            <tr>
              <th scope='row' style={{background: '#bfbfbf'}}></th>
              <td>Space Left</td>
              <td>{this.state.surplusDetails[2]}</td>
            </tr>
          </tbody>
        </table>
        <Selling drizzle = {this.props.drizzle} drizzleState = {this.props.drizzleState} battery={this.state.surplusDetails}/>
      </div>);
  }
}

export default GetSurplus;
