import React, {Component} from 'react'
import ReactLoading from 'react-loading'
import jwt_decode from 'jwt-decode'

import GetSurplus from "./GetSurplusDetails";
import GetDemand from "./GetDemandDetails";
import SetBatteryCharge from "./SetBatteryCharge";

class HouseStatus extends Component {
    constructor(){
        super();
        this.state = {
            loading: true,
            drizzleState: null,
            type: '',
        };
    }

    //when component mounts get the user type and set the state for type loading and drizzle
    componentDidMount(){
        const token = localStorage.getItem('usertoken')
        if(token != null && !this.state.didUpdate ){
            this.setState({didUpdate: true});
            const decode = jwt_decode(token)
            this.setState({
                type: decode.identity.type,
            })
        }

        this.setState({
            loading: this.props.loading,
            drizzleState: this.props.drizzleState,
        })
    }

    //render component
    render() {
        const Loading = () => {
            return <div class="text-center" ><ReactLoading type={"spinningBubbles"} color={"#F9C232"}/></div>
        }

        const Surplus = (props) => {
            if(this.state.type == 1 && this.state.drizzleState != null){
                return(
                    <div>
                        <GetSurplus
                          drizzle={this.props.drizzle}
                          drizzleState={this.state.drizzleState}
                         />
                    </div>
                );
            }else if(this.state.type == 1) {
                return <Loading />;
            } else {
                return null
            }
        }

        const Demand = (props) => {
            if(this.state.type == 2 && this.state.drizzleState != null){
                return(
                    <div>
                        <GetDemand
                          drizzle={this.props.drizzle}
                          drizzleState={this.state.drizzleState}
                         />
                    </div>
                );
            }else if(this.state.type == 2){
                return <Loading />;
            } else {
                return null
            }
        }

        if (this.state.loading) return <div style={{paddingTop: 100}}>Loading Drizzle...</div>;
        return (
            <div className="container" style={{paddingTop: 100}}>
                <div className="jumbotron mt-5 text-center">
                    <h1 className="h3 mb-3 font-weight-normal">House Status</h1>
                    <div class="d-flex justify-content-center">
                        <Demand />
                        <Surplus />
                 </div>
                 <div class="d-flex justify-content-center">
                     <SetBatteryCharge
                        drizzle={this.props.drizzle}
                        drizzleState={this.state.drizzleState}
                     />
                 </div>
             </div>
         </div>
        )
    }
}

export default HouseStatus;
