import React from "react";

class SetSpendTokens extends React.Component {
  constructor() {
  super()
  this.state = {
      stackId: null,
      tokenAmount: 0,
   };

     this.handleChange = this.handleChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
   }

   //on change handle state change
   handleChange(e) {
       this.setState({tokenAmount: e.target.value});
   }

   //on submit call setSTValue
   handleSubmit() {
       //write to blockchain
       this.setSTValue(this.state.tokenAmount);
   }

   //function to write to the blockchain
  setSTValue(amount) {
      const { drizzle, drizzleState } = this.props;
      const contract = drizzle.contracts.powerchainger;
      const token = amount * 1000;

      const stackId = contract.methods["spendTokens"].cacheSend(token, {from: drizzleState.accounts[0]});

       this.setState({ stackId: stackId});
  }



  getTxStatus = () => {
    // get the transaction states from the drizzle state
    const { transactions, transactionStack } = this.props.drizzleState;

    // get the transaction hash using our saved `stackId`
    const txHash = transactionStack[this.state.stackId];

    // if transaction hash does not exist, don't display anything
    if (!txHash) return null;

    // otherwise, return the transaction status
    return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
  };
            //<div>{this.getTxStatus()}</div>

  //render component
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
            <table>
            <tr>
            <label>
                <td>Amount:</td>
                <td><input type="text" class="form-control" name="tokenAmount" onChange={this.handleChange} value={this.state.tokenAmount} /> </td>
            </label>
            </tr>
            <tr>
                <td><input type="submit" class="btn btn-primary" value="submit" /></td>
            </tr>
            </table>
        </form>
      </div>
    );
  }
}

export default SetSpendTokens;
