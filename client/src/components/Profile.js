import React, {Component} from 'react'
import ReactLoading from 'react-loading'
import jwt_decode from 'jwt-decode'

import { browserHistory } from 'history'

import {Surplus, Demand, Battery} from "./classes"

import $ from 'jquery'

import pic from './picture.jpg'

import {Link, withRouter} from 'react-router-dom'
import { FaSignInAlt, FaUserPlus, FaHome, FaChartPie, FaWallet, FaHandHoldingHeart, FaBatteryHalf, FaCoins } from "react-icons/fa";

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            type: '',
            loading: true,
            drizzleState: null,
            status: '',
        }
    }

    //when component mounts get the user data from the usertoken
    componentDidMount(){
        console.log("mounted");
        const token = localStorage.usertoken
        const decode = jwt_decode(token)
        this.setState({
            first_name: decode.identity.first_name,
            last_name: decode.identity.last_name,
            email: decode.identity.email,
            type: decode.identity.type
        })
    }

    //render component
    render() {
      const useStyles = makeStyles({
        card: {
          minWidth: 357,
          color: '#A09C40',
          backgroundColor: "#479FB2",
        },
        media: {
          height: 50,
          color: 'white',
        },
        picture: {
          width: 235,
          height: 235,
          paddingLeft: 55,
          color: 'white',
        },
      });

      const DonateMediaCard = () => {
        const classes = useStyles();

        return (
          <Card className={classes.card}>
            <Link to="/donate" style={{textDecoration: 'none', color: 'black'}}>
            <CardActionArea>
              <CardContent>
                <Typography style={{color: 'white'}} gutterBottom variant="h5" component="h2">
                  Donate Energy
                </Typography>
                <Typography className={classes.media} variant="body2" color="textSecondary" component="p">
                  Click on this card to donate your energy to a demand household
                </Typography>
                <FaHandHoldingHeart className={classes.picture}/>
              </CardContent>
            </CardActionArea>
            </Link>
          </Card>
        );
      }

      const BuyMediaCard = () => {
        const classes = useStyles();

        return (
          <Card className={classes.card}>
            <Link to="/donate" style={{textDecoration: 'none', color: 'black'}}>
            <CardActionArea>
              <CardContent>
                <Typography style={{color: 'white'}} gutterBottom variant="h5" component="h2">
                  Buy Energy
                </Typography>
                <Typography className={classes.media} variant="body2" color="textSecondary" component="p">
                  Click on this card to buy your energy from the neigbourhood
                  battery
                </Typography>
                <FaCoins className={classes.picture}/>
              </CardContent>
            </CardActionArea>
            </Link>
          </Card>
        );
      }

      const HouseMediaCard = () => {
        const classes = useStyles();

        return (
          <Card className={classes.card}>
            <Link to="/housestatus" style={{textDecoration: 'none', color: 'black'}}>
            <CardActionArea>
              <CardContent>
                <Typography style={{color: 'white'}} gutterBottom variant="h5" component="h2">
                  House Status
                </Typography>
                <Typography className={classes.media} variant="body2" color="textSecondary" component="p">
                  Click on this card to see your own house status and the amount
                  of energy you have donated
                </Typography>
                <FaHome className={classes.picture}/>
              </CardContent>
            </CardActionArea>
            </Link>
          </Card>
        );
      }

      const HomeMediaCard = () => {
        const classes = useStyles();

        return (
          <Card className={classes.card}>
            <Link to="/" style={{textDecoration: 'none', color: 'black'}}>
            <CardActionArea>
              <CardContent>
                <Typography style={{color: 'white'}} gutterBottom variant="h5" component="h2">
                  Home
                </Typography>
                <Typography className={classes.media} variant="body2" color="textSecondary" component="p">
                  Click this card to go back to the homepage.
                </Typography>
                <FaHome className={classes.picture}/>
              </CardContent>
            </CardActionArea>
            </Link>
          </Card>
        );
      }

      const NeigbourhoodMediaCard = () => {
        const classes = useStyles();

        return (
          <Card className={classes.card}>
            <Link to="/neigbourhoodstatus" style={{textDecoration: 'none', color: 'black'}}>
            <CardActionArea>
              <CardContent>
                <Typography style={{color: 'white'}} gutterBottom variant="h5" component="h2">
                  Neigbourhood status
                </Typography>
                <Typography className={classes.media} variant="body2" color="textSecondary" component="p">
                  Click this card to see the status of the neigbourhood battery
                </Typography>
                <FaChartPie className={classes.picture}/>
              </CardContent>
            </CardActionArea>
            </Link>
          </Card>
        );
      }

      const TransMediaCard = () => {
        const classes = useStyles();

        return (
          <Card className={classes.card}>
            <Link to="/transactions" style={{textDecoration: 'none', color: 'black'}}>
            <CardActionArea>
              <CardContent>
                <Typography style={{color: 'white'}} gutterBottom variant="h5" component="h2">
                  Transactions
                </Typography>
                <Typography className={classes.media} variant="body2" color="textSecondary" component="p">
                  Click this card to see an overview of the transactions you
                  have made.
                </Typography>
                <FaWallet className={classes.picture}/>
              </CardContent>
            </CardActionArea>
            </Link>
          </Card>
        );
      }

      const LogoutMediaCard = () => {
        const classes = useStyles();

        return (
          <Card className={classes.card}>
            <Link style={{textDecoration: 'none', color: 'black'}}>
            <CardActionArea>
              <CardContent>
                <Typography style={{color: 'white'}} gutterBottom variant="h5" component="h2">
                  Logout
                </Typography>
                <Typography className={classes.media} variant="body2" color="textSecondary" component="p">
                  Click this card to see an overview of the transactions you
                  have made.
                </Typography>
                <FaHome className={classes.picture}/>
              </CardContent>
            </CardActionArea>
            </Link>
          </Card>
        );
      }

      const AddBatteryMediaCard = () => {
        const classes = useStyles();

        return (
          <Card className={classes.card}>
            <Link to="/addbattery" style={{textDecoration: 'none', color: 'black'}}>
            <CardActionArea>
              <CardContent>
                <Typography style={{color: 'white'}} gutterBottom variant="h5" component="h2">
                  Add Battery
                </Typography>
                <Typography className={classes.media} variant="body2" color="textSecondary" component="p">
                  Click this card to be able to add a neigbourhood battery to
                  the network
                </Typography>
                <FaBatteryHalf className={classes.picture}/>
              </CardContent>
            </CardActionArea>
            </Link>
          </Card>
        );
      }

      const AddHouseHoldMediaCard = () => {
        const classes = useStyles();

        return (
          <Card className={classes.card}>
            <Link to="/addhouse" style={{textDecoration: 'none', color: 'black'}}>
            <CardActionArea>
              <CardContent>
                <Typography style={{color: 'white'}} gutterBottom variant="h5" component="h2">
                  Add Household
                </Typography>
                <Typography className={classes.media} variant="body2" color="textSecondary" component="p">
                  Click this card to be able to add a surplus or demand
                  household to the network
                </Typography>
                <FaHome className={classes.picture}/>
              </CardContent>
            </CardActionArea>
            </Link>
          </Card>
        );
      }

      const SurplusCards = () => {
          return (
              <div>
              <div className="row">
                  <div class="col-sm"></div>
                  <div className="col-sm">
                      <DonateMediaCard />
                  </div>
                  <div className="col-sm">
                      <HouseMediaCard />
                  </div>
                  <div class="col-sm"></div>
              </div>
              <div className="spacer" style={{paddingTop:30}}></div>
              <div className="row">
                  <div className="col">
                  </div>
                  <div className="col">
                      <NeigbourhoodMediaCard />
                  </div>
                  <div className="col">
                      <TransMediaCard />
                  </div>
                  <div className="col">
                  </div>
              </div>
              </div>
          )
      }

      const DemandCards = () => {
          return (
              <div>
              <div className="row">
                  <div class="col-sm"></div>
                  <div className="col-sm">
                      <BuyMediaCard />
                  </div>
                  <div className="col-sm">
                      <HouseMediaCard />
                  </div>
                  <div class="col-sm"></div>
              </div>
              <div className="spacer" style={{paddingTop:30}}></div>
              <div className="row">
                  <div className="col">
                  </div>
                  <div className="col">
                      <NeigbourhoodMediaCard />
                  </div>
                  <div className="col">
                      <TransMediaCard />
                  </div>
                  <div className="col">
                  </div>
              </div>
              </div>
          )
      }

      const EfundCards = () => {
          return (
              <div>
              <div className="row">
                  <div class="col-sm"></div>
                  <div className="col-sm">
                      <AddBatteryMediaCard />
                  </div>
                  <div className="col-sm">
                      <AddHouseHoldMediaCard />
                  </div>
                  <div class="col-sm"></div>
              </div>
              <div className="spacer" style={{paddingTop:30}}></div>
              <div className="row">
                  <div className="col">
                  </div>
                  <div className="col">
                      <NeigbourhoodMediaCard />
                  </div>
                  <div className="col">
                      <TransMediaCard />
                  </div>
                  <div className="col">
                  </div>
              </div>
              </div>
          )
      }

      const Cards = () => {
          switch(this.state.type) {
              case 1:
                return <SurplusCards />
                break;
              case 2:
                return <DemandCards />
                break;
              case 3:
                return <EfundCards />
                break;
              default:
                return null
          }
      }

            return (
                <div style={{paddingTop: 100}}>
                <div className="pic" style={{background: "url(" + pic + ")", height: '600px', justifyContent: 'center', alignItems: 'center'}}>
                    <img src={pic} style={{position: 'absolute', height: 600, width: '100%'}} />
                        <div className="container center text-center" style={{zIndex: 10, position: 'relative', color: 'white', justifyContent: 'center', alignItems: 'center', paddingTop: 25}}>
                            <h1>POWERCHAINGER</h1>
                            <h3>WEB-BASED SOCIAL ENERGY MARKET</h3>
                        </div>
                    </div>
                    <div className="container" style={{paddingTop: 50}}>
                        <Cards />
                    </div>
            </div>
        );
    }
}

export default Profile
