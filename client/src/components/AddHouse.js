import React, {Component} from 'react'

import SetHousehold from "./SetHousehold";

class AddHouse extends Component {
    constructor(){
        super();
        this.state = {
            loading: true,
            drizzleState: null,
        };
    }

    //when component mounts set the state for loading and drizzle
    componentDidMount(){
        this.setState({
            loading: this.props.loading,
            drizzleState: this.props.drizzleState,
        })
    }
    
    //render component
    render() {
        if (this.state.loading) return "Loading Drizzle...";
        return (
            <div className="container">
                <div className="jumbotron mt-5 text-center">
                    <h1 className="h3 mb-3 font-weight-normal">Add a Household to the network</h1>
                    <div class="d-flex justify-content-center">
                        <SetHousehold
                          drizzle={this.props.drizzle}
                          drizzleState={this.state.drizzleState}
                         />
                 </div>
             </div>
         </div>
        )
    }
}

export default AddHouse;
