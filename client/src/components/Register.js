import React, {Component} from 'react'
import {register} from './UserFunctions'
import { Redirect } from 'react-router'

class Register extends Component {
    constructor(){
        super()
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            phone_number: '',
            address: '',
            zip_code: '',
            city: '',
            type: '',
            redirect: false,
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    //function to change state from the form
    onChange(e){
        this.setState({[e.target.name]: e.target.value})
    }

    //set the type of user that is registering
    setType(e) {
        this.setState({type: e.target.value})
        console.log(e.target.value)
        console.log(this.state.type)
    }

    //on submit call the register function
    onSubmit(e){
        e.preventDefault()

        const newUser = {
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            email: this.state.email,
            password: this.state.password,
            phone_number: this.state.phone_number,
            address: this.state.address,
            zip_code: this.state.zip_code,
            city: this.state.city,
            type: this.state.type,
        }

        register(newUser).then(res => {
            this.setState({redirect: true})
            //this.props.history.push('/login')
        })
    }

    //render component
    render() {
        if(this.state.redirect) {
            return <Redirect to='/' />
        }
        return (
            <div className="container">
                <div className="jumbotron mt-5">
                    <div className="row">
                        <div className="col-md-6 mt-5 mx-auto">
                            <form noValidate onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label htmlFor="first_name">First name: </label>
                                    <input type="first_name" className="form-control" name="first_name" placeholder="Enter first name" value={this.state.first_name} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="last_name">Last name: </label>
                                    <input type="last_name" className="form-control" name="last_name" placeholder="Enter last name" value={this.state.last_name} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">Email: </label>
                                    <input type="email" className="form-control" name="email" placeholder="Enter email" value={this.state.email} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Password: </label>
                                    <input type="password" className="form-control" name="password" placeholder="Enter password" value={this.state.password} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="phone_number">Phone number: </label>
                                    <input type="phone_number" className="form-control" name="phone_number" placeholder="Enter phone number" value={this.state.phone_number} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="address">Address: </label>
                                    <input type="address" className="form-control" name="address" placeholder="Enter address" value={this.state.address} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="zip_code">Zip code: </label>
                                    <input type="zip_code" className="form-control" name="zip_code" placeholder="Enter zip code" value={this.state.zip_code} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="city">City: </label>
                                    <input type="city" className="form-control" name="city" placeholder="Enter city" value={this.state.city} onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="type">Register as a: </label>
                                    <div onChange={this.setType.bind(this)}>
                                        <table className='text-left'>
                                            <tr><td><input type="radio" value="1" name="type"/></td> <td>Surplus household</td></tr>
                                            <tr><td><input type="radio" value="2" name="type"/></td> <td>Demand household</td></tr>
                                            <tr><td><input type="radio" value="3" name="type"/></td> <td>Energy fund</td></tr>
                                            <tr><td><input type="radio" value="4" name="type"/></td> <td>Network operator</td></tr>
                                        </table>
                                    </div>
                                </div>
                                <button type="submit" className="btn btn-lg btn-primary btn-block">Register</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Register
