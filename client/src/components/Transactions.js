import React, {Component} from 'react'
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import jwt_decode from 'jwt-decode'

import SetSpendTokens from "./SetSpendTokens";
import GetDonationids from "./GetDonationids";
import GetRedemptionids from "./GetRedemptionids";

class Transactions extends Component {
    constructor(){
        super();
        this.state = {
            loading: true,
            drizzleState: null,
            type: null,
        };
    }

    //when component mounts check user type and handle state changes
    componentDidMount(){
        const token = localStorage.getItem('usertoken')
        if(token != null && !this.state.didUpdate ){
            this.setState({didUpdate: true});
            const decode = jwt_decode(token)
            this.setState({
                type: decode.identity.type,
            })
        }
        this.setState({
            loading: this.props.loading,
            drizzleState: this.props.drizzleState,
        })
    }

    //render component
    render() {
        if (this.state.loading) return "Loading Drizzle...";
        else if(this.state.type === 1) {
            return (
                <div className="container">
                    <div className="jumbotron mt-5 text-center">
                        <h1 className="h3 mb-3 font-weight-normal">See transactions you have made</h1>
                        <div class="d-flex justify-content-center">
                            <GetDonationids
                                drizzle={this.props.drizzle}
                                drizzleState={this.state.drizzleState}
                            />
                     </div>
                 </div>
             </div>
            )
        }
        else if(this.state.type === 2) {
            return (
                <div className="container">
                    <div className="jumbotron mt-5 text-center">
                        <h1 className="h3 mb-3 font-weight-normal">See Redemptions you have made</h1>
                        <div class="d-flex justify-content-center">
                            <GetRedemptionids
                                drizzle={this.props.drizzle}
                                drizzleState={this.state.drizzleState}
                            />
                     </div>
                 </div>
             </div>
            )
        } else {
            return null;
        }
    }
}

export default Transactions;
