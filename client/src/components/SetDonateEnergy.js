import React from "react";

class SetDonateEnergy extends React.Component {
  constructor(props) {
  super(props)
  this.state = {
      stackId: null,
      energyAmount: 0,
      totalEnergyAmount: 0,
      dish: [false, 1, 0],
      coffee: [false, 1, 0],
      tv: [false, 1, 0],
   };
     this.handleChange = this.handleChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
     this.handleChecked = this.handleChecked.bind(this);
     this.handleMultiplier = this.handleMultiplier.bind(this);
   }

   //on change set the state for energyAmount
   handleChange(e) {
       this.setState({energyAmount: e.target.value});
   }


   handleChange(e) {
       var name = e.target.getAttribute('name')
       this.setState({[name]: e.target.value});
   }

   //when compont updates check if amount has changed
   componentDidUpdate() {
       var amount = 0;
       amount += this.state.dish[2]
       amount += this.state.coffee[2]
       amount += this.state.tv[2]
       amount += Number(this.state.energyAmount)
       amount = Number((amount).toFixed(3))
       if(this.state.totalEnergyAmount !== amount){
           this.setState({totalEnergyAmount: amount})
       }

   }

   //check if and option has been selected
   handleChecked (e) {
       var name = e.target.getAttribute('name');
       console.log(name);
       var state
       var amount = 0
       switch(name) {
        case 'dish':
            if(this.state.dish[0] === false){
                amount = 1.5 * this.state.dish[1];
                state = [!this.state.dish[0], this.state.dish[1], amount]
            } else {
                state = [!this.state.dish[0], this.state.dish[1], 0]
            }
            break;
        case 'coffee':
            if(this.state.coffee[0] === false){
                amount = 0.12 * this.state.coffee[1];
                state = [!this.state.coffee[0], this.state.coffee[1], amount]
            } else {
                state = [!this.state.coffee[0], this.state.coffee[1], 0]
            }
            break;
        case 'tv':
            if(this.state.tv[0] === false){
                amount = 0.012 * this.state.tv[1];
                state = [!this.state.tv[0], this.state.tv[1], amount]
            } else {
                state = [!this.state.tv[0], this.state.tv[1], 0]
            }
            break;
        }
       this.setState({[name]: state});
    }

    //check if a multiplier is added
    handleMultiplier (e) {
        var name = e.target.getAttribute('name');
        var state, amount
        switch(name) {
         case 'dish':
             if(this.state.dish[0] === true){
                 amount = 1.5 * e.target.value;
                 state = [this.state.dish[0], e.target.value, amount]
             } else {
                 state = [this.state.dish[0], e.target.value, this.state.dish[2]]
             }
             break;
         case 'coffee':
             if(this.state.coffee[0] === true){
                 amount = 0.12 * e.target.value;
                 state = [this.state.coffee[0], e.target.value, amount]
             } else {
                 state = [this.state.coffee[0], e.target.value, this.state.coffee[2]]
             }
             break;
         case 'tv':
             if(this.state.tv[0] === true){
                 amount = 0.012 * e.target.value;
                 state = [this.state.tv[0], e.target.value, amount]
             } else {
                 state = [this.state.tv[0], e.target.value, this.state.tv[2]]
             }
             break;
         }
        this.setState({[name]: state});
    }

    //on submit call setDEValue
   handleSubmit() {
       this.setDEValue(this.state.totalEnergyAmount);
   }

   //set donated energy into the blockchain
  setDEValue(amount) {
      const { drizzle, drizzleState } = this.props;
      const contract = drizzle.contracts.powerchainger;

      const energy = amount * 1000;

      const stackId = contract.methods["donateEnergy"].cacheSend(energy, {from: drizzleState.accounts[0]});

       this.setState({ stackId: stackId});
  }



  getTxStatus = () => {
    // get the transaction states from the drizzle state
    const { transactions, transactionStack } = this.props.drizzleState;

    // get the transaction hash using our saved `stackId`
    const txHash = transactionStack[this.state.stackId];

    // if transaction hash does not exist, don't display anything
    if (!txHash) return null;

    // otherwise, return the transaction status
    return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
  };
            //<div>{this.getTxStatus()}</div>
  //render component
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
            <table className="table">
            <tr>
            <label>
                <td>Custom Amount:</td>
                <td><input type="text" class="form-control" name="energyAmount" onChange={this.handleChange} value={this.state.energyAmount} /> </td>
            </label>
            </tr>
            </table>
            <table className="table">
                <thead>
                    <th scope='col'>Appliance</th>
                    <th scope='col'>Select</th>
                    <th scope='col'>Multiplier</th>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">Dishwash:</th>
                        <td><input type="checkbox" name="dish" onChange={this.handleChecked} value={1.5}/>≈1.5kWh</td>
                        <td>
                        <select type="text" class="form-control" name="dish" onChange={this.handleMultiplier} value={this.state.dish[1]}>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Coffee machine:</th>
                        <td><input type="checkbox" name="coffee" onChange={this.handleChecked} value={0.12}/>≈0.12kWh</td>
                        <td>
                        <select type="text" class="form-control" name="coffee" onChange={this.handleMultiplier} value={this.state.coffee[1]}>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">LCD TV:</th>
                        <td><input type="checkbox" name="tv" onChange={this.handleChecked} value={0.012} />≈0.012kWh</td>
                        <td>
                        <select type="text" class="form-control" name="tv" onChange={this.handleMultiplier} value={this.state.tv[1]}>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            Amount of energy you are about to donate: {this.state.totalEnergyAmount} kWh <br/><br/>
            <input type="submit" class="btn btn-primary" value="submit" />
        </form>
      </div>
    );
  }
}

export default SetDonateEnergy;
