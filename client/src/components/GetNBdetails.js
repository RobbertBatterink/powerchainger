import React from "react";
import PieChart from 'react-minimal-pie-chart';

class GetBattery extends React.Component {
    constructor(){
        super();
        this.state = {
            dataKey: null,
            batteryStatus: [100, 50, 50, 0, 0, 0],
         };

        this.getBattery = this.getBattery.bind(this);
    }

    //when component mouts call the getBattery function
    componentDidMount() {
        this.getBattery();
    }

    //function to get the neigbourhoodstatus
  getBattery() {
    const { drizzle } = this.props;
    const contract = drizzle.contracts.powerchainger;

    // let drizzle know we want to call the `getNBdetails` method
    const dataKey = contract.methods.getNBdetails().call();
    //parse data so we can use it
    var promise1 = Promise.resolve(dataKey);
    promise1.then(function(value) {
        return value;
    }).then(data => {
        this.setState({batteryStatus: [data[0] / 1000, data[1] / 1000, data[2] / 1000, data[3] / 1000, data[4] / 1000, data[5] / 1000]});
    });

    // save the `dataKey` to local component state for later reference
    this.setState({ dataKey: dataKey });
  }

  //render component
  render() {
    return (
        <div>
            <h2>Battery Size: {this.state.batteryStatus[0]} kWh</h2>
            <h2>Energy donated: {this.state.batteryStatus[3]} kWh</h2>
            <h2>Total Tokens Spend: {this.state.batteryStatus[4]} Tokens</h2>
            <h2>Total Tokens Unspend: {this.state.batteryStatus[5]} Tokens</h2>

              <PieChart
                    data={[
                        { title: "Space Left", value: this.state.batteryStatus[2], color: '#bfbfbf' },
                        { title: "Current Charge", value: this.state.batteryStatus[1], color: '#19D209' },
                    ]}
                    totalValue={this.state.batteryStatus[0]}
                    lineWidth={20}
                    label={({ data, dataIndex }) =>
                        Math.round(data[dataIndex].percentage) + '%'
                    }
                    labelStyle={{
                        fontSize: '10px',
                        fontFamily: 'sans-serif'
                    }}
                    style={{height: '500px'}}
                    labelPosition={60}
                    rounded
                    animate
                />
                <h2>Legend</h2>
                <table className='table'>
                  <thead>
                    <tr>
                      <th scope='col'>Color</th>
                      <th scope='col'>Name</th>
                      <th scope='col'>Value</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope='row' style={{backgroundColor: '#19D209'}}></th>
                      <td>Current Charge</td>
                      <td>{this.state.batteryStatus[1]}</td>
                    </tr>
                    <tr>
                      <th scope='row' style={{background: '#bfbfbf'}}></th>
                      <td>Space Left</td>
                      <td>{this.state.batteryStatus[2]}</td>
                    </tr>
                  </tbody>
                </table>
        </div>);
  }
}

export default GetBattery;
