import React from "react";

class SetBattery extends React.Component {
  constructor() {
  super()
  this.state = {
      stackId: null,
      batterySize: 0,
      batteryCharge: 0,
   };

     this.handleChange = this.handleChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
   }

   //on change set the state
   handleChange(e) {
       var name = e.target.getAttribute('name');
       this.setState({[name]: e.target.value});

   }

   //on submit call setNHvalue function
   handleSubmit() {
       //write to blockchain
       const size = this.state.batterySize * 1000;
       const charge = this.state.batteryCharge *1000;

       this.setNBValue(size, charge);

   }

   //function to set the battery
  setNBValue(batterySize, batteryCharge) {
      const { drizzle, drizzleState } = this.props;
      const contract = drizzle.contracts.powerchainger;

      const stackId = contract.methods["addNbattery"].cacheSend(batterySize, batteryCharge, {from: drizzleState.accounts[0]});

       this.setState({stackId: stackId});
  }



  getTxStatus = () => {
    // get the transaction states from the drizzle state
    const { transactions, transactionStack } = this.props.drizzleState;

    // get the transaction hash using our saved `stackId`
    const txHash = transactionStack[this.state.stackId];

    // if transaction hash does not exist, don't display anything
    if (!txHash) return null;

    // otherwise, return the transaction status
    return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
  };
            //<div>{this.getTxStatus()}</div>
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
            <table>
            <tr>
            <label>
                <td>Battery size:</td>
                <td><input type="text" class="form-control" name="batterySize" onChange={this.handleChange} value={this.state.batterySize} /> </td>
            </label>
            </tr>
            <tr>
            <label>
                <td>Battery charge:</td>
                <td><input type="text" class="form-control" name="batteryCharge" onChange={this.handleChange} value={this.state.batteryCharge} /> </td>
            </label>
            </tr>
            <tr>
                <td><input type="submit" class="btn btn-primary" value="submit" /></td>
            </tr>
            </table>
        </form>
      </div>
    );
  }
}

export default SetBattery;
