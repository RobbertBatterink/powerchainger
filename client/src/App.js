import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom'

import Navbar from './components/Navbar'
import Footer from './components/Footer'
import Login from './components/Login'
import Register from './components/Register'
import Profile from './components/Profile'
import Buying from './components/Buying'
import Transactions from './components/Transactions'
import HouseStatus from './components/HouseStatus'
import NeigbourhoodStatus from './components/NeighbourStatus'
import Selling from './components/Selling'
import AddBattery from './components/AddBattery'
import AddHouse from './components/AddHouse'
import NewLanding from './components/NewLanding'



class App extends Component {
    state = { loading: true, drizzleState: null };

    //when component mounts make a connection to the store
    componentDidMount() {
      const { drizzle } = this.props;
      console.log(drizzle)
      // subscribe to changes in the store
      this.unsubscribe = drizzle.store.subscribe(() => {

        // every time the store updates, grab the state from drizzle
        const drizzleState = drizzle.store.getState();
        // check to see if it's ready, if so, update local component state
        if (drizzleState.drizzleStatus.initialized) {
          this.setState({ loading: false, drizzleState });
        }
      });
    }

    //on unmount unsubscribe from the store
    compomentWillUnmount() {
      this.unsubscribe();
    }

    //render component
  render() {
      const PNewLanding = (props) => {
          return (<NewLanding history={this.props.history} />)
      }
      const MyProfile = (props) => {
          return (<Profile drizzle = {this.props.drizzle} history={this.props.history}/>);
      }

      const DrizzleBuying = (props) => {
          return (<Buying drizzle = {this.props.drizzle} loading = {this.state.loading} drizzleState = {this.state.drizzleState} />);
      }

      const DrizzleTransactions = (props) => {
          return (<Transactions drizzle = {this.props.drizzle} loading = {this.state.loading} drizzleState = {this.state.drizzleState} />);
      }

      const DrizzleNeigbourhood = (props) => {
          return (<NeigbourhoodStatus drizzle = {this.props.drizzle} loading = {this.state.loading} drizzleState = {this.state.drizzleState} />);
      }

      const DrizzleHouseStatus = (props) => {
          return (<HouseStatus drizzle = {this.props.drizzle} loading = {this.state.loading} drizzleState = {this.state.drizzleState} />);
      }

      const DrizzleSelling = (props) => {
          return (<Selling drizzle = {this.props.drizzle} loading = {this.state.loading} drizzleState = {this.state.drizzleState} />);
      }

      const DrizzleAddBattery = (props) => {
          return (<AddBattery drizzle = {this.props.drizzle} loading = {this.state.loading} drizzleState = {this.state.drizzleState} />);
      }

      const DrizzleAddHouse = (props) => {
          return (<AddHouse drizzle = {this.props.drizzle} loading = {this.state.loading} drizzleState = {this.state.drizzleState} />);
      }
    return (
        <Router>
            <div className="App" style={{minHeight: 1000}}>
                <Navbar />
                <Route exact path="/" component={PNewLanding} />
                <Route exact path="/profile" component={MyProfile} />
                <div className="container" style={{paddingBottom: 50}}>
                    <Route exact path="/register" component={Register} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/buy" component={DrizzleBuying} />
                    <Route exact path="/transactions" component={DrizzleTransactions} />
                    <Route exact path="/neigbourhoodstatus" component={DrizzleNeigbourhood} />
                    <Route exact path="/housestatus" component={DrizzleHouseStatus} />
                    <Route exact path="/donate" component={DrizzleSelling} />
                    <Route exact path="/addbattery" component={DrizzleAddBattery} />
                    <Route exact path="/addhouse" component={DrizzleAddHouse} />
                </div>
            </div>
            <Footer />
        </Router>
    );
  }
}

export default App;
