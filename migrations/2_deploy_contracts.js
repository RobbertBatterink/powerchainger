const MyStringStore = artifacts.require("MyStringStore");
const Powerchainger = artifacts.require("Powerchainger");

module.exports = function(deployer) {
  deployer.deploy(MyStringStore);
  deployer.deploy(Powerchainger);
};
