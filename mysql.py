from flask import Flask, jsonify, request, render_template
from flask_mysqldb import MySQL
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_jwt_extended import create_access_token

app = Flask(__name__)

app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'powerchainger'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
app.config['JWT_SECRET_KEY'] = 'secret'

mysql = MySQL(app)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)
CORS(app)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/users/register', methods=['POST'])
def register():
    cur = mysql.connection.cursor()
    first_name = request.get_json()['first_name']
    last_name = request.get_json()['last_name']
    email = request.get_json()['email']
    password = bcrypt.generate_password_hash(request.get_json()['password']).decode('utf-8')
    phone_number = request.get_json()['phone_number']
    address = request.get_json()['address']
    zip_code = request.get_json()['zip_code']
    city = request.get_json()['city']
    type = request.get_json()['type']

    cur.execute("INSERT INTO users (first_name, last_name, email, password, phone_number, address, zip_code, city, type) VALUES ('" +
    str(first_name) + "','" +
    str(last_name) + "','" +
    str(email) + "','" +
    str(password) + "','" +
    str(phone_number) + "','" +
    str(address) + "','" +
    str(zip_code) + "','" +
    str(city) + "','" +
    str(type) + "')")

    mysql.connection.commit()

    result = {
        "first_name" : first_name,
        "last_name" : last_name,
        "email" : email,
        "password" : password,
        "phone_number" : phone_number,
        "address" : address,
        "zip_code" : zip_code,
        "city" : city,
        "type" : type,
    }

    return jsonify({"result" : result})

@app.route('/users/login', methods=['POST'])
def login():
    cur = mysql.connection.cursor()
    email = request.get_json()['email']
    password = request.get_json()['password']
    result = ""

    cur.execute("SELECT * FROM users WHERE email = '" + str(email) + "'")
    rv = cur.fetchone()

    if rv != None and bcrypt.check_password_hash(rv['password'], password):
        access_token = create_access_token(identity = {'first_name' : rv['first_name'], 'last_name' : rv['last_name'], 'email' : rv['email'], 'type' : rv['type']})
        result = jsonify({"token":access_token})
    else:
        result = jsonify({"err" : "Invalid email or password"})

    return result

if __name__ == '__main__':
    app.run(debug=True)
